{-# LANGUAGE DeriveGeneric   #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE TemplateHaskell #-}

module NSQ where

import           Data.Aeson
import           Data.Aeson.Casing          (aesonPrefix, snakeCase)
import qualified Data.ByteString.Lazy       as BSL
import           Data.List                  as L
import           Data.String
import           Data.Text                  as T
import           Data.Time
import           Network.HTTP.Conduit
import           Protolude
import           System.Environment         as SE

-- cassava
import           Data.Csv                   (FromRecord, Header, ToNamedRecord,
                                             ToRecord)
import qualified Data.Csv                   as Cassava

-- vector
import qualified Data.Vector                as Vector

-- dotenv
import qualified Configuration.Dotenv       as Dotenv
import           Configuration.Dotenv.Types

-- Dotenv.loadFile $ Config {configPath = [".env"], configExamplePath = [], configOverride = False}

-- | Client data type.
data Client = Client
  { clientClientId                      :: Text -- ^ Client ID.
  , clientHostname                      :: Text -- ^ Hostname of the client.
  , clientVersion                       :: Text -- ^ Client version.
  , clientRemoteAddress                 :: Text -- ^ Remote address of client.
  , clientState                         :: Int -- ^ State of client.
  , clientReadyCount                    :: Int -- ^ Amount of ready messages.
  , clientInFlightCount                 :: Int -- ^ Amount of flight messages.
  , clientMessageCount                  :: Int -- ^ Amount of messages.
  , clientFinishCount                   :: Int -- ^ Amount of finished messages.
  , clientRequeueCount                  :: Int -- ^ Amount of requeued messages.
  , clientConnectTs                     :: Int
  , clientSampleRate                    :: Int
  , clientDeflate                       :: Bool
  , clientSnappy                        :: Bool
  , clientUserAgent                     :: Text
  , clientTls                           :: Bool
  , clientTlsCipherSuite                :: Text
  , clientTlsVersion                    :: Text
  , clientTlsNegotiatedProtocol         :: Text
  , clientTlsNegotiatedProtocolIsMutual :: Bool
  } deriving (Show, Generic)

instance ToJSON Client where
  toJSON = genericToJSON $ aesonPrefix snakeCase

instance FromJSON Client where
  parseJSON = genericParseJSON $ aesonPrefix snakeCase

-- | Data type for channel.
data Channel = Channel
  { channelChannelName   :: Text -- ^ The name of the channel.
  , channelDepth         :: Int
  , channelBackendDepth  :: Int
  , channelInFlightCount :: Int -- ^ Amount of flight messages.
  , channelDeferredCount :: Int -- ^ Amount of deferred messages.
  , channelMessageCount  :: Int -- ^ Amount of messages.
  , channelRequeueCount  :: Int -- ^ Amount of requeued messages.
  , channelTimeoutCount  :: Int
  , channelClients       :: [Client]
  , channelPaused        :: Bool
  } deriving (Show, Generic)

instance ToJSON Channel where
  toJSON = genericToJSON $ aesonPrefix snakeCase

instance FromJSON Channel where
  parseJSON = genericParseJSON $ aesonPrefix snakeCase

data Topic = Topic
  { topicTopicName    :: Text
  , topicChannels     :: [Channel]
  , topicDepth        :: Int
  , topicBackendDepth :: Int
  , topicMessageCount :: Int
  , topicPaused       :: Bool
  } deriving (Show, Generic)

instance ToJSON Topic where
  toJSON = genericToJSON $ aesonPrefix snakeCase

instance FromJSON Topic where
  parseJSON = genericParseJSON $ aesonPrefix snakeCase

data Statistic = Statistic
  { statisticVersion   :: Text
  , statisticHealth    :: Text
  , statisticTopics    :: [Topic]
  , statisticStartTime :: Int
  } deriving (Show, Generic)

instance ToJSON Statistic where
  toJSON = genericToJSON $ aesonPrefix snakeCase

instance FromJSON Statistic where
  parseJSON = genericParseJSON $ aesonPrefix snakeCase

data Report = Report
  { reportTopicName    :: Text
  , reportMessage      :: Int
  , reportTotalMessage :: Int
  , reportConnection   :: Int
  , reportDate         :: Text
  , reportTime         :: Text
  } deriving (Generic, Show)

instance FromRecord Report

instance ToRecord Report

instance ToNamedRecord Report where
  toNamedRecord Report {..} =
    Cassava.namedRecord
      [ "Topic Name" Cassava..= reportTopicName
      , "Message" Cassava..= reportMessage
      , "Total Message" Cassava..= reportTotalMessage
      , "Connection" Cassava..= reportConnection
      , "Date" Cassava..= reportDate
      , "Time" Cassava..= reportTime
      ]

processNsq :: Text -> IO ()
processNsq topicName = do
  nsqStatisticURL <- SE.getEnv "NSQ_STAT_URL"
  let statisticURL topic channel =
        nsqStatisticURL ++ "?format=json&topic=" ++ topic ++ "&channel=" ++ channel

  jsonString <- lookupNsqStatistic $ statisticURL (unpack topicName) "job"
  let statistic = eitherDecode jsonString :: Either String Statistic
  case statistic of
    Left err   -> putStrLn err
    Right stat -> processTopics stat topicName

lookupNsqStatistic :: String -> IO BSL.ByteString
lookupNsqStatistic url = simpleHttp url

processTopics :: Statistic -> Text -> IO ()
processTopics statistic topicName = do
  let topics = statisticTopics statistic
      topic = filterTopicName topics topicName
      channels =
        case topic of
          Just t  -> topicChannels t :: [Channel]
          Nothing -> []
  -- reports <- populateReport topic channels
  reports <- alternativePopulateReport topic channels
  reportToCSV reports
  sendSlackNotification

-- | Get `Topic` from array of `Topic`.
filterTopicName :: [Topic] -> Text -> Maybe Topic
filterTopicName topics topicName =
  case L.filter (\x -> topicTopicName x == topicName) topics of
    t:_ -> Just t
    _   -> Nothing

-- | Get `Channel` from array of `Channel`.
filterChannelName :: [Channel] -> Text -> Maybe Channel
filterChannelName channels channelName =
  case L.filter (\x -> channelChannelName x == channelName) channels of
    t:_ -> Just t
    _   -> Nothing

-- | Iterate the `Channel`s to get the `Report`s.
populateReport :: Maybe Topic -> [Channel] -> IO [Report]
populateReport _ [] = return []
populateReport Nothing _ = return []
populateReport (Just topic) (x:xs) = do
  formattedDate <- getFormattedDate
  formattedTime <- getFormattedTime
  report <- populateReport (Just topic) xs
  return $
    [ Report
        (topicTopicName topic)
        (channelDepth x)
        (channelMessageCount x)
        (L.length $ channelClients x)
        formattedDate
        formattedTime
    ] ++
    report

-- | Map over the `Channel`s to get the `Report`s.
alternativePopulateReport :: Maybe Topic -> [Channel] -> IO [Report]
alternativePopulateReport Nothing _ = return []
alternativePopulateReport (Just topic) chans = mapM (populRep topic) chans
  where
    populRep t channel = do
      _ <- Dotenv.loadFile defaultConfig
      favoriteanimal <- getEnv "FAVORITE_ANIMAL"
      putStrLn favoriteanimal
      formattedDate <- getFormattedDate
      formattedTime <- getFormattedTime
      return $
        Report
          (topicTopicName t)
          (channelDepth channel)
          (channelMessageCount channel)
          (L.length $ channelClients channel)
          formattedDate
          formattedTime

-- | Get date which has been formatted into string.
getFormattedDate :: IO Text
getFormattedDate = do
  currentLocalTime <- getZonedTime
  return $ T.pack $ formatTime defaultTimeLocale "%Y-%m-%d" currentLocalTime

-- | Get time which has been formatted into string.
getFormattedTime :: IO Text
getFormattedTime = do
  currentLocalTime <- getZonedTime
  return $ T.pack $ formatTime defaultTimeLocale "%H:%M" currentLocalTime

reportHeader :: Header
reportHeader =
  Vector.fromList
    ["Topic Name", "Message", "Total Message", "Connection", "Date", "Time"]

-- | Put `Report`s into csv file.
reportToCSV :: [Report] -> IO ()
reportToCSV reports = do
  BSL.writeFile "NSQ Statistic Report.csv" $
    Cassava.encodeByName reportHeader reports

-- | Send notification to slack channel using web hook.
sendSlackNotification :: IO ()
sendSlackNotification = do
  manager <- newManager tlsManagerSettings
  let requestObject =
        object
          [ "name" .= ("NSQ Statistic Checker" :: String)
          , "text" .= ("NSQ Statistic Report has been compiled" :: String)
          ]

  slackURL <- SE.getEnv "SLACK_URL"
  initialRequest <- parseRequest slackURL

  let request =
        initialRequest
        { method = "POST"
        , requestHeaders = [("Content-Type", "application/json")]
        , requestBody = RequestBodyLBS $ encode requestObject
        }
  response <- httpLbs request manager
  putStrLn $ responseBody response
