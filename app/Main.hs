{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE OverloadedStrings #-}

module Main where

import           Protolude

import qualified Configuration.Dotenv       as Dotenv
import           Configuration.Dotenv.Types
import           NSQ
import           System.Environment
import           Turtle

-- Dotenv.loadFile $ Config {configPath = [".env"], configExamplePath = [], configOverride = False}

-- print text subcommand
printText :: (Maybe Int, Text) -> IO ()
printText (Nothing, text')  = echo (unsafeTextToLine text')
printText ((Just i), text') = replicateM_ i (echo (unsafeTextToLine text'))

printArgs :: Parser (Maybe Int, Text)
printArgs =
  (,) <$> optional (optInt "times" 'n' "Number of times") <*>
  argText "text" "Text to print"

parsePrintArgs :: Parser (IO ())
parsePrintArgs =
  fmap
    printText
    (subcommand
       "print"
       "Print specified text in specified number of times"
       printArgs)

-- topic subcommand
parseProcessNsq :: Parser (IO ())
parseProcessNsq =
  fmap
    processNsq
    (subcommand
       "topic"
       "Process specified topic"
       (argText "topic_name" "What topic should be processed"))

-- main command
mainCommand :: IO ()
mainCommand = echo "Use \"-h\" argument for help"

parseMainCommand :: Parser (IO ())
parseMainCommand = pure mainCommand

-- main parser
parser :: Parser (IO ())
parser = parseMainCommand <|> parseProcessNsq <|> parsePrintArgs

-- main
main :: IO ()
main = do
  -- cmd <- options "NSQ Statistic Checker" parser
  -- Karena baca konfigurasi dilakukan saat pertama kali jalan,
  -- maka `Dotenv` digunakan pada `main`.
  -- Bisa juga diletakkan pada `src/NSQ.hs:201`
  _ <- Dotenv.loadFile defaultConfig
  favoriteanimal <- getEnv "FAVORITE_ANIMAL"
  Protolude.putStrLn favoriteanimal
  -- cmd
