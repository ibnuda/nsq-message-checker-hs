# nsq-message-checker

This program will compile nsq statistic for specific topic into CSV and send notification to user via Slack web hook.
Processing multiple topic is still on progress.

## Setup

```shell
# Setup project
stack setup

# Do an initial build of the project
stack build

# Needed for haskero (vscode plugin)
stack build intero

# Needed for Haskell GHCi Debug Adapter Phoityne (vscode plugin)
stack install phoityne-vscode

# Build the project
stack build
```

## Usage

```shell
stack exec nsq-message-checker -- topic "topic_name"
```